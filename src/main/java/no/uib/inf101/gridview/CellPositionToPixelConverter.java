package no.uib.inf101.gridview;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;


import java.awt.geom.Rectangle2D;

public class CellPositionToPixelConverter {

  Rectangle2D box;
  GridDimension gd;
  double margin;

  public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin){
    this.box = box;
    this.gd = gd;
    this.margin = margin;
  }

  public Rectangle2D getBoundsForCell(CellPosition cp){
    double cellWidth =(box.getWidth()-((gd.cols()+1)*margin))/gd.cols();
    double cellHeight =(box.getHeight()-((gd.rows()+1)*margin))/gd.rows();
    double cellX = box.getX()+margin*(cp.col()+1)+cellWidth*(cp.col());
    double cellY = box.getY()+margin*(cp.row()+1)+cellHeight*(cp.row());
    
    Rectangle2D rect = new Rectangle2D.Double(cellX, cellY, cellWidth, cellHeight);

    return rect;
  }
}
