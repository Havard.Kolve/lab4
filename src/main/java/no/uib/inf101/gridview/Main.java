package no.uib.inf101.gridview;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;
import no.uib.inf101.colorgrid.IColorGrid;
import java.awt.Color;

public class Main {
  public static void main(String[] args) {
    // Opprett et rutenett med 3 rader og 4 kolonner
    ColorGrid obj = new ColorGrid(3,4);

    obj.set(new CellPosition(0,0), Color.RED);
    obj.set(new CellPosition(0, 3), Color.BLUE);
    obj.set(new CellPosition(2, 0), Color.YELLOW);
    obj.set(new CellPosition(2, 3), Color.GREEN);


  }
}
