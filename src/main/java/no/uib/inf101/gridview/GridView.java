package no.uib.inf101.gridview;


import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.IColorGrid;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;


public class GridView extends JPanel {
  private final IColorGrid grid;
  CellColorCollection c;

  public GridView(IColorGrid grid){
    this.grid = grid;
    this.setPreferredSize(new Dimension(400,300));
  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    this.drawGrid(g2);
  }
  
  private static final double OUTERMARGIN = 30;
  private static final double GRIDMARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;

  private void drawGrid(Graphics2D g2) {
    Rectangle2D shape1 = new Rectangle2D.Double(OUTERMARGIN, OUTERMARGIN, this.getWidth()-2*OUTERMARGIN, this.getHeight()-2*OUTERMARGIN);
    g2.setColor(MARGINCOLOR);
    g2.fill(shape1);

    CellPositionToPixelConverter object = new CellPositionToPixelConverter(shape1, this.grid, GRIDMARGIN);
    
    drawCells(g2, grid, object);
  }

  private static void drawCells(Graphics2D g, CellColorCollection c, CellPositionToPixelConverter pp){

    for (CellColor element : c.getCells()) {
      Rectangle2D rect = pp.getBoundsForCell(element.cellPosition());
      g.draw(rect);
      if (element.color() == (null)){
        g.setColor(Color.DARK_GRAY);
        g.fill(rect);
      } else {
        g.setColor(element.color());
        g.fill(rect);
      }
    }
  }
}
