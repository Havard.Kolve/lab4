package no.uib.inf101.colorgrid;

import java.util.ArrayList;
import java.util.List;
import java.awt.Color;

public class ColorGrid implements IColorGrid{
  int rows;
  int cols;
  CellPosition pos;

  Color color;

  List<ArrayList<Color>> grid;

  List<CellColor> Cells= new ArrayList<CellColor>();
  
  public ColorGrid(int rows , int cols) {
    this.rows = rows;
    this.cols = cols;
    this.grid = new ArrayList<ArrayList<Color>>();

    for (int i =0; i < rows; i++){
      grid.add(new ArrayList<>());

    }
    for (int i =0; i < rows; i++){
      for (int j =0; j < cols; j++){
        grid.get(i).add(null);
    }
    }
  }


  @Override
  public Color get(CellPosition pos){

    if (pos.row() < 0 || pos.col() < 0 || pos.row()>= rows() || pos.col()>= cols()){
      throw new IndexOutOfBoundsException();
    }
    
    int row = pos.row();
    int col = pos.col();

    return grid.get(row).get(col);
    
  }




  @Override
  public void set(CellPosition pos, Color color){
    int row = pos.row();
    int col = pos.col(); 

    grid.get(row).set(col, color);

    if (pos.row() < 0 || pos.col() < 0 || pos.row()>= rows() || pos.col()>= cols()){
      throw new IndexOutOfBoundsException();
    }
  }
  @Override
  public int rows(){
    return this.rows;
  }

  @Override
  public int cols(){
    return this.cols;
  }
  @Override
  public List<CellColor> getCells() {
    List<CellColor> cells = new ArrayList<CellColor>();
    for (int i = 0; i < rows(); i++){

      for (int j = 0; j < cols(); j++){

        CellPosition pos = new CellPosition(i, j);

        cells.add(new CellColor(pos, get(pos)));
      }
    }
    return cells;
  }
}
